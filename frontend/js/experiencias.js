
function loadData(){
    let request = sendRequest('experiencia/list', 'GET', '')
    let table = document.getElementById('experiencias-table');
    table.innerHTML = "";
    request.onload = function(){
        let data = request.response;
        console.log(data);
        data.forEach(element => {
            table.innerHTML += `
                <tr>
                    <th>${element.idExperiencia}</th>
                    <td>${element.tipoUsuario}</td>
                    <td>${element.fecha}</td>
                    <td>${element.descripcion}</td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "form_experiencias.html?id=${element.idExperiencia}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteExperiencia(${element.idExperiencia})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos. loadData</td>
            </tr>
        `;
    }
}

function loadExperiencia(idExperiencia){
    let request = sendRequest('experiencia/list/'+idExperiencia, 'GET', '')
    let user = document.getElementById('experiencia-user')
    let date = document.getElementById('experiencia-date')
    let expl = document.getElementById('experiencia-expl')
    let id = document.getElementById('experiencia-id')
    request.onload = function(){
        
        let data = request.response
        id.value = data.idExperiencia
        user.value = data.tipoUsuario
        date.value = data.fecha
        expl.value = data.descripcion
    }
    request.onerror = function(){
        alert("Error al recuperar los datos. loadExperiencia");
    }
}

function deleteExperiencia(idExperiencia){
    let request = sendRequest('experiencia/'+idExperiencia, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveExperiencia(){
    let user = document.getElementById('experiencia-user').value
    let date = document.getElementById('experiencia-date').value
    let expl = document.getElementById('experiencia-expl').value
    let id = document.getElementById('experiencia-id').value
    let data = {'idExperiencia': id,'tipoUsuario':user,'fecha': date, 'descripcion': expl }
    let request = sendRequest('experiencia/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'experiencias.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios. saveExperiencia')
    }
}