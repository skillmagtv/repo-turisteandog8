
function loadData(){
    let request = sendRequest('usuario/list', 'GET', '')
    let table = document.getElementById('usuarios-table');
    table.innerHTML = "";
    request.onload = function(){
        let data = request.response;
        console.log(data);
        data.forEach(element => {
            table.innerHTML += `
                <tr>
                    <th>${element.idUsuario}</th>
                    <td>${element.userName}</td>
                    <td>${element.nombre}</td>
                    <td>${element.tipoDocumento}</td>
					<td>${element.contraseña}</td>
					<td>${element.tipoUsuario}</td>
					<td>${element.correo}</td>
					<td>${element.celular}</td>
					<td>${element.PaisOrigen}</td>
					<td>${element.ciudad}</td>
					<td>${element.direccion}</td>
					<td>${element.rnt}</td>
					<td>${element.idioma}</td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "form_usuarios.html?id=${element.idUsuario}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteUsuario(${element.idUsuario})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos. loadData</td>
            </tr>
        `;
    }
}

function loadUsuario(idUsuario){
    let request = sendRequest('usuario/list/'+idUsuario, 'GET', '')
    let id = document.getElementById('usuario-id')
    let userName = document.getElementById('usuario-userName')
    let nombre = document.getElementById('usuario-nombre')
    let tipoDocumento = document.getElementById('usuario-tipoDocumento')
    let contraseña = document.getElementById('usuario-contraseña')
    let tipoUsuario = document.getElementById('usuario-tipoUsuario')
    let correo = document.getElementById('usuario-correo')
    let celular = document.getElementById('usuario-celular')
    let PaisOrigen = document.getElementById('usuario-PaisOrigen')
    let ciudad = document.getElementById('usuario-ciudad')
    let direccion = document.getElementById('usuario-direccion')
    let rnt = document.getElementById('usuario-rnt')
    let idioma = document.getElementById('usuario-idioma')
    request.onload = function(){
        
        let data = request.response
        id.value = data.idUsuario
        userName.value = data.userName
        nombre.value = data.nombre
        tipoDocumento.value = data.tipoDocumento
        contraseña.value = data.contraseña
        tipoUsuario.value = data.tipoUsuario
        correo.value = data.value
        celular.value = data.value
        PaisOrigen.value = data.PaisOrigen
        ciudad.value = data.ciudad
        direccion.value = data.direccion
        rnt.value = data.rnt
        idioma.value = idioma.value

    }
    request.onerror = function(){
        alert("Error al recuperar los datos. loadUsuario");
    }
}

function deleteUsuario(idUsuario){
    let request = sendRequest('usuario/'+idUsuario, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveUsuario(){

    let id = document.getElementById('usuario-id')
    let userName = document.getElementById('usuario-userName')
    let nombre = document.getElementById('usuario-nombre')
    let tipoDocumento = document.getElementById('usuario-tipoDocumento')
    let contraseña = document.getElementById('usuario-contraseña')
    let tipoUsuario = document.getElementById('usuario-tipoUsuario')
    let correo = document.getElementById('usuario-correo')
    let celular = document.getElementById('usuario-celular')
    let PaisOrigen = document.getElementById('usuario-PaisOrigen')
    let ciudad = document.getElementById('usuario-ciudad')
    let direccion = document.getElementById('usuario-direccion')
    let rnt = document.getElementById('usuario-rnt')
    let idioma = document.getElementById('usuario-idioma')
    let data = {'id': id,'userName': userName,'nombre': nombre, 'tipoDocumento': tipoDocumento, 'contraseña': contraseña, 'tipoUsuario': tipoUsuario, 'correo': correo, 'celular': celular, 'PaisOrigen': PaisOrigen, 'ciudad': ciudad, 'direccion': direccion, 'rnt': rnt , 'idioma': idioma}
    let request = sendRequest('Usuario/', id ? 'PUT' : 'POST', data)



    request.onload = function(){
        window.location = 'usuarios.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios. saveUsuario')
    }
}