
function loadData(){
    let request = sendRequest('atraccion/list', 'GET', '')
    let table = document.getElementById('atraccions-table');
    table.innerHTML = "";
    request.onload = function(){
        let data = request.response;
        console.log(data);
        data.forEach(element => {
            table.innerHTML += `
                <tr>
                    <th>${element.idAtraccion}</th>
                    <td>${element.idCiudadAtraccion}</td>
                    <td>${element.descripcion}</td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "form_atracciones.html?id=${element.idAtraccion}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteAtraccion(${element.idAtraccion})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos. loadData</td>
            </tr>
        `;
    }
}

function loadAtraccion(idAtraccion){
    let request = sendRequest('atraccion/list/'+idAtraccion, 'GET', '')
    let city = document.getElementById('atraccion-city')
    let expl = document.getElementById('atraccion-expl')
    let id = document.getElementById('atraccion-id')
    request.onload = function(){
        
        let data = request.response
        id.value = data.idAtraccion
        city.value = data.idCiudadAtraccion
        expl.value = data.descripcion
    }
    request.onerror = function(){
        alert("Error al recuperar los datos. loadAtraccion");
    }
}

function deleteAtraccion(idAtraccion){
    let request = sendRequest('atraccion/'+idAtraccion, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveAtraccion(){
    let city = document.getElementById('atraccion-city').value
    let expl = document.getElementById('atraccion-expl').value
    let id = document.getElementById('atraccion-id').value
    let data = {'idAtraccion': id,'idCiudadAtraccion': city,'descripcion': expl }
    let request = sendRequest('atraccion/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'atracciones.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios. saveAtraccion')
    }
}