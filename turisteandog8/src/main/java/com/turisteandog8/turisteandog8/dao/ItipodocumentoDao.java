/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.turisteandog8.turisteandog8.dao;

import com.turisteandog8.turisteandog8.modelo.tipodocumento;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author mateo
 */
public interface ItipodocumentoDao extends 
        CrudRepository<tipodocumento, Integer>{
    
}
