/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.turisteandog8.turisteandog8.dao;

import com.turisteandog8.turisteandog8.modelo.departamento;
import org.springframework.data.repository.CrudRepository;
/**
 *
 * @author David
 */
public interface IdepartamentoDao extends CrudRepository<departamento, Integer> {
    
}
