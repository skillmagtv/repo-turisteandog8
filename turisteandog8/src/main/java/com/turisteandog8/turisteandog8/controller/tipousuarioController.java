/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.turisteandog8.turisteandog8.controller;

import com.turisteandog8.turisteandog8.modelo.tipousuario;
import com.turisteandog8.turisteandog8.services.tipousuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 *
 * @author nicol
 */
@CrossOrigin(origins = {"http://localhost:4406", "*"})
@RestController
@RequestMapping("/tipousuario")
public class tipousuarioController {
    @Autowired
    private tipousuarioService tipousuarioService;
    
    @PostMapping(value = "/")
    public ResponseEntity<tipousuario> agregar(@RequestBody tipousuario tipousuario) {
        tipousuario obj = tipousuarioService.save(tipousuario);
    return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<tipousuario> eliminar(@PathVariable Integer id) {
        tipousuario obj = tipousuarioService.findById(id);
        if (obj != null) {
        tipousuarioService.delete(id);
        } else {
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @PutMapping(value = "/")
    public ResponseEntity<tipousuario> editar(@RequestBody tipousuario producto) {
        tipousuario obj = tipousuarioService.findById(producto.getIdTipoUsuario());
        if (obj != null) {
        obj.setDescripcion(producto.getDescripcion());
        tipousuarioService.save(obj);
        } else {
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @GetMapping("/list")
    public List<tipousuario> consultarTodo() {
        return tipousuarioService.findAll();
    }
    @GetMapping("/list/{id}")
    public tipousuario consultaPorId(@PathVariable Integer id) {
        return tipousuarioService.findById(id);
    }
}
