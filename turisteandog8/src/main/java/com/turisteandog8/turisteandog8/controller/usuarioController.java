/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.turisteandog8.turisteandog8.controller;

import com.turisteandog8.turisteandog8.modelo.usuario;
import com.turisteandog8.turisteandog8.services.usuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author nicol
 */
@CrossOrigin(origins = {"http://localhost:4406", "*"})
@RestController
@RequestMapping("/usuario")
public class usuarioController {
        
    @Autowired
    private usuarioService usuarioService;
    
    @PostMapping(value = "/")
    public ResponseEntity<usuario> agregar(@RequestBody usuario usuario) {
        usuario obj = usuarioService.save(usuario);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<usuario> eliminar(@PathVariable Integer id) {
        usuario obj = usuarioService.findById(id);
        if (obj != null) {
        usuarioService.delete(id);
        } else {
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @PutMapping(value = "/")
    public ResponseEntity<usuario> editar(@RequestBody usuario usuario) {
        usuario obj = usuarioService.findById(usuario.getIdUsuario());
        if (obj != null) {
        obj.setUserName(usuario.getUserName());
        obj.setNombre(usuario.getNombre());
        obj.setTipoDocumento(usuario.getTipoDocumento());
        obj.setContraseña(usuario.getContraseña());
        obj.setTipoUsuario(usuario.getTipoUsuario());
        obj.setCorreo(usuario.getCorreo());
        obj.setCelular(usuario.getCelular());
        obj.setPaisOrigen(usuario.getPaisOrigen());
        obj.setCiudad(usuario.getCiudad());
        obj.setDireccion(usuario.getDireccion());
        obj.setRnt(usuario.getRnt());
        obj.setIdioma(usuario.getIdioma());
        usuarioService.save(obj);
        } else {
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @GetMapping("/list")
    public List<usuario> consultarTodo() {
        return usuarioService.findAll();
    }
    @GetMapping("/list/{id}")
    public usuario consultaPorId(@PathVariable Integer id) {
        return usuarioService.findById(id);
    }
    
}
