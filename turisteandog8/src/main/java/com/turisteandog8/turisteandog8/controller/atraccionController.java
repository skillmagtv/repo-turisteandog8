/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.turisteandog8.turisteandog8.controller;

import com.turisteandog8.turisteandog8.modelo.atraccion;
import com.turisteandog8.turisteandog8.modelo.ciudad;
import com.turisteandog8.turisteandog8.services.atraccionService;
import com.turisteandog8.turisteandog8.services.ciudadService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author nicol
 */
@CrossOrigin(origins = {"http://localhost:4406", "*"})
@RestController
@RequestMapping("/atraccion")
public class atraccionController {
    @Autowired
    private atraccionService atraccionService;
    
    @PostMapping(value = "/")
    public ResponseEntity<atraccion> agregar(@RequestBody atraccion atraccion) {
        atraccion obj = atraccionService.save(atraccion);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<atraccion> eliminar(@PathVariable Integer id) {
        atraccion obj = atraccionService.findById(id);
        if (obj != null) {
        atraccionService.delete(id);
        } else {
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @PutMapping(value = "/")
    public ResponseEntity<atraccion> editar(@RequestBody atraccion atraccion) {
        atraccion obj = atraccionService.findById(atraccion.getIdAtraccion());
        if (obj != null) {
        obj.setIdCiudadAtraccion(atraccion.getIdCiudadAtraccion());
        obj.setDescripcion(atraccion.getDescripcion());
        atraccionService.save(obj);
        } else {
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @GetMapping("/list")
    public List<atraccion> consultarTodo() {
        return atraccionService.findAll();
    }
    @GetMapping("/list/{id}")
    public atraccion consultaPorId(@PathVariable Integer id) {
        return atraccionService.findById(id);
    }
}
