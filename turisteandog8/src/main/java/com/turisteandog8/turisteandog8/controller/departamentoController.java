/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.turisteandog8.turisteandog8.controller;

import com.turisteandog8.turisteandog8.modelo.departamento;
import com.turisteandog8.turisteandog8.services.departamentoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author David
 */

@CrossOrigin(origins = {"http://localhost:4406", "*"})
@RestController
@RequestMapping("/departamento")


public class departamentoController {
    
    @Autowired
    private departamentoService departamentoService;
    
    @PostMapping(value = "/")
    public ResponseEntity<departamento> agregar(@RequestBody departamento departamento) {
        departamento obj = departamentoService.save(departamento);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<departamento> eliminar(@PathVariable Integer id) {
        departamento obj = departamentoService.findById(id);
        if (obj != null) {
        departamentoService.delete(id);
        } else {
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @PutMapping(value = "/")
    public ResponseEntity<departamento> editar(@RequestBody departamento departamento) {
        departamento obj = departamentoService.findById(departamento.getIdDepto());
        if (obj != null) {
        obj.setDescripcion(departamento.getDescripcion());
        departamentoService.save(obj);
        } else {
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @GetMapping("/list")
    public List<departamento> consultarTodo() {
        return departamentoService.findAll();
    }
    @GetMapping("/list/{id}")
    public departamento consultaPorId(@PathVariable Integer id) {
        return departamentoService.findById(id);
    }
    
    
    
}
