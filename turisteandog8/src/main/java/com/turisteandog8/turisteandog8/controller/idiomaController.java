/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.turisteandog8.turisteandog8.controller;

import com.turisteandog8.turisteandog8.modelo.idioma;
import com.turisteandog8.turisteandog8.services.idiomaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author nicol
 */
@CrossOrigin(origins = {"http://localhost:4406", "*"})
@RestController
@RequestMapping("/idioma")
public class idiomaController {
    @Autowired
    private idiomaService idiomaService;
    
    @PostMapping(value = "/")
    public ResponseEntity<idioma> agregar(@RequestBody idioma idioma) {
        idioma obj = idiomaService.save(idioma);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<idioma> eliminar(@PathVariable Integer id) {
        idioma obj = idiomaService.findById(id);
        if (obj != null) {
        idiomaService.delete(id);
        } else {
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @PutMapping(value = "/")
    public ResponseEntity<idioma> editar(@RequestBody idioma idioma) {
        idioma obj = idiomaService.findById(idioma.getIdidioma());
        if (obj != null) {
        obj.setDescripcion(idioma.getDescripcion());
        idiomaService.save(obj);
        } else {
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @GetMapping("/list")
    public List<idioma> consultarTodo() {
        return idiomaService.findAll();
    }
    @GetMapping("/list/{id}")
    public idioma consultaPorId(@PathVariable Integer id) {
        return idiomaService.findById(id);
    }
}
