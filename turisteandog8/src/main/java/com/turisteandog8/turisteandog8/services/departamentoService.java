/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.turisteandog8.turisteandog8.services;

import com.turisteandog8.turisteandog8.modelo.departamento;
import java.util.List;
/**
 *
 * @author David
 */
public interface departamentoService {
    
    public departamento save(departamento departamento);
    public void delete(Integer id); 
    public departamento findById(Integer id); 
    public List<departamento> findAll();
    
}
