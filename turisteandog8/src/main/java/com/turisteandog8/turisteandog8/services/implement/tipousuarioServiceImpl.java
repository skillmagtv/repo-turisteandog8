/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.turisteandog8.turisteandog8.services.implement;

import com.turisteandog8.turisteandog8.dao.ItipousuarioDao;
import com.turisteandog8.turisteandog8.modelo.tipousuario;
import com.turisteandog8.turisteandog8.services.tipousuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author nicol
 */
@Service
public class tipousuarioServiceImpl implements tipousuarioService{
    @Autowired
    private ItipousuarioDao ItipousuarioDao;
   
    //Guardar el producto
    @Override
    @Transactional (readOnly = false)
    public tipousuario save(tipousuario tipousuario) {
    return ItipousuarioDao.save(tipousuario);
    }
    //Borrar producto
    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
    ItipousuarioDao.deleteById(id);
    }
    //Buscar por ID
    @Override
    @Transactional (readOnly = true)
    public tipousuario findById(Integer id) {
    return ItipousuarioDao.findById(id).orElse(null);
    }
    //Listar todos los productos
    @Override
    @Transactional (readOnly = true)
    public List<tipousuario> findAll() {
    return (List<tipousuario>) ItipousuarioDao.findAll();
    }
}
