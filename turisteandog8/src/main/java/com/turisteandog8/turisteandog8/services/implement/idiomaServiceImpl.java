/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.turisteandog8.turisteandog8.services.implement;

import com.turisteandog8.turisteandog8.dao.IidiomaDao;
import com.turisteandog8.turisteandog8.modelo.idioma;
import com.turisteandog8.turisteandog8.services.idiomaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author nicol
 */
@Service
public class idiomaServiceImpl implements idiomaService{
    @Autowired
    private IidiomaDao IidiomaDao;
    //Guardar el producto
    @Override
    @Transactional (readOnly = false)
    public idioma save(idioma idioma) {
    return IidiomaDao.save(idioma);
    }
    //Borrar producto
    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
    IidiomaDao.deleteById(id);
    }
    //Buscar por ID
    @Override
    @Transactional (readOnly = true)
    public idioma findById(Integer id) {
    return IidiomaDao.findById(id).orElse(null);
    }
    //Listar todos los productos
    @Override
    @Transactional (readOnly = true)
    public List<idioma> findAll() {
    return (List<idioma>) IidiomaDao.findAll();
    }
}
