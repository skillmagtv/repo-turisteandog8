/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.turisteandog8.turisteandog8.services;

import com.turisteandog8.turisteandog8.modelo.idioma;
import java.util.List;

/**
 *
 * @author nicol
 */
public interface idiomaService {
    public idioma save(idioma idioma); 
    public void delete(Integer id); 
    public idioma findById(Integer id); 
    public List<idioma> findAll();
}
