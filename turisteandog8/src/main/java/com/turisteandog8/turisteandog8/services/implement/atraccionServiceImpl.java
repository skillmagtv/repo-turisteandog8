/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.turisteandog8.turisteandog8.services.implement;

import com.turisteandog8.turisteandog8.dao.IatraccionDao;
import com.turisteandog8.turisteandog8.modelo.atraccion;
import com.turisteandog8.turisteandog8.services.atraccionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author nicol
 */
@Service
public class atraccionServiceImpl implements atraccionService{
        
    @Autowired
    private IatraccionDao IatraccionDao;
    //Guardar el producto
    @Override
    @Transactional (readOnly = false)
    public atraccion save(atraccion atraccion) {
    return IatraccionDao.save(atraccion);
    }
    //Borrar producto
    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
    IatraccionDao.deleteById(id);
    }
     //Buscar por ID
    @Override
    @Transactional (readOnly = true)
    public atraccion findById(Integer id) {
    return IatraccionDao.findById(id).orElse(null);
    }
    //Listar todos los productos
    @Override
    @Transactional (readOnly = true)
    public List<atraccion> findAll() {
    return (List<atraccion>) IatraccionDao.findAll();
    }
}
