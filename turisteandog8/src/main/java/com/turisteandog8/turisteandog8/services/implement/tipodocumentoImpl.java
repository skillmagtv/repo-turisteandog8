/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.turisteandog8.turisteandog8.services.implement;

import com.turisteandog8.turisteandog8.dao.ItipodocumentoDao;
import com.turisteandog8.turisteandog8.modelo.tipodocumento;
import com.turisteandog8.turisteandog8.services.tipodocumentoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author nicol
 */
@Service
public class tipodocumentoImpl implements tipodocumentoService{
    @Autowired
    private ItipodocumentoDao ItipodocumentoDao;
   
    //Guardar el producto
    @Override
    @Transactional (readOnly = false)
    public tipodocumento save(tipodocumento tipodocumento) {
    return ItipodocumentoDao.save(tipodocumento);
    }
    //Borrar producto
    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
    ItipodocumentoDao.deleteById(id);
    }
    //Buscar por ID
    @Override
    @Transactional (readOnly = true)
    public tipodocumento findById(Integer id) {
    return ItipodocumentoDao.findById(id).orElse(null);
    }
    //Listar todos los productos
    @Override
    @Transactional (readOnly = true)
    public List<tipodocumento> findAll() {
    return (List<tipodocumento>) ItipodocumentoDao.findAll();
    }
}
