/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.turisteandog8.turisteandog8.services;

import com.turisteandog8.turisteandog8.modelo.usuario;
import java.util.List;

/**
 *
 * @author nicol
 */
public interface usuarioService {
    public usuario save (usuario usuario);
    public void delete (Integer id);
    public usuario findById (Integer id);
    public List<usuario> findAll();
}
