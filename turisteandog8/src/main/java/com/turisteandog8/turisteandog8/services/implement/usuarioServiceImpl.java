/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.turisteandog8.turisteandog8.services.implement;

import com.turisteandog8.turisteandog8.dao.IusuarioDao;
import com.turisteandog8.turisteandog8.modelo.usuario;
import com.turisteandog8.turisteandog8.services.usuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author David
 */

@Service
public class usuarioServiceImpl implements usuarioService {
    
    @Autowired
    private IusuarioDao IusuarioDao;
    //Guardar el producto
    @Override
    @Transactional (readOnly = false)
    public usuario save(usuario usuario) {
    return IusuarioDao.save(usuario);
    }
    //Borrar producto
    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
    IusuarioDao.deleteById(id);
    }
     //Buscar por ID
    @Override
    @Transactional (readOnly = true)
    public usuario findById(Integer id) {
    return IusuarioDao.findById(id).orElse(null);
    }
    //Listar todos los productos
    @Override
    @Transactional (readOnly = true)
    public List<usuario> findAll() {
    return (List<usuario>) IusuarioDao.findAll();
    }
}
