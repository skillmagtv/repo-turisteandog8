/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.turisteandog8.turisteandog8.services.implement;

import com.turisteandog8.turisteandog8.dao.IexperienciaDao;
import com.turisteandog8.turisteandog8.modelo.experiencia;
import com.turisteandog8.turisteandog8.services.experienciaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author nicol
 */
@Service
public class experienciaServiceImpl implements experienciaService{
    @Autowired
    private IexperienciaDao IexperienciaDao;
   
    //Guardar el producto
    @Override
    @Transactional (readOnly = false)
    public experiencia save(experiencia experiencia) {
    return IexperienciaDao.save(experiencia);
    }
    //Borrar producto
    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
    IexperienciaDao.deleteById(id);
    }
    //Buscar por ID
    @Override
    @Transactional (readOnly = true)
    public experiencia findById(Integer id) {
    return IexperienciaDao.findById(id).orElse(null);
    }
    //Listar todos los productos
    @Override
    @Transactional (readOnly = true)
    public List<experiencia> findAll() {
    return (List<experiencia>) IexperienciaDao.findAll();
    }
}
