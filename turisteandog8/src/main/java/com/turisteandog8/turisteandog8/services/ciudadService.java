/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.turisteandog8.turisteandog8.services;

import com.turisteandog8.turisteandog8.modelo.ciudad;
import java.util.List;

/**
 *
 * @author David
 */
public interface ciudadService {
    
    public ciudad save(ciudad ciudad);
    public void delete(Integer id); 
    public ciudad findById(Integer id); 
    public List<ciudad> findAll();
    
}
