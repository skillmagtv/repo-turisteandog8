/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.turisteandog8.turisteandog8.services.implement;

import com.turisteandog8.turisteandog8.dao.IdepartamentoDao;
import com.turisteandog8.turisteandog8.modelo.departamento;
import com.turisteandog8.turisteandog8.services.departamentoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author David
 */

@Service
public class departamentoServiceImpl implements departamentoService{

    @Autowired
    private IdepartamentoDao IdepartamentoDao;
    //Guardar el producto
    @Override
    @Transactional (readOnly = false)
    public departamento save(departamento departamento) {
    return IdepartamentoDao.save(departamento);
    }
    //Borrar producto
    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
    IdepartamentoDao.deleteById(id);
    }
     //Buscar por ID
    @Override
    @Transactional (readOnly = true)
    public departamento findById(Integer id) {
    return IdepartamentoDao.findById(id).orElse(null);
    }
    //Listar todos los productos
    @Override
    @Transactional (readOnly = true)
    public List<departamento> findAll() {
    return (List<departamento>) IdepartamentoDao.findAll();
    }
    
    
}
