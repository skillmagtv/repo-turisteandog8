/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.turisteandog8.turisteandog8.services;

import com.turisteandog8.turisteandog8.modelo.atraccion;
import java.util.List;

/**
 *
 * @author nicol
 */
public interface atraccionService {
    public atraccion save(atraccion atraccion);
    public void delete(Integer id); 
    public atraccion findById(Integer id); 
    public List<atraccion> findAll();
}
