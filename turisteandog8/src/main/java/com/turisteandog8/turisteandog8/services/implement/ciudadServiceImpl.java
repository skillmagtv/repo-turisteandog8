/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.turisteandog8.turisteandog8.services.implement;

import com.turisteandog8.turisteandog8.dao.IciudadDao;
import com.turisteandog8.turisteandog8.modelo.ciudad;
import com.turisteandog8.turisteandog8.services.ciudadService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author David
 */

@Service
public class ciudadServiceImpl implements ciudadService {
    
    @Autowired
    private IciudadDao IciudadDao;
    //Guardar el producto
    @Override
    @Transactional (readOnly = false)
    public ciudad save(ciudad ciudad) {
    return IciudadDao.save(ciudad);
    }
    //Borrar producto
    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
    IciudadDao.deleteById(id);
    }
     //Buscar por ID
    @Override
    @Transactional (readOnly = true)
    public ciudad findById(Integer id) {
    return IciudadDao.findById(id).orElse(null);
    }
    //Listar todos los productos
    @Override
    @Transactional (readOnly = true)
    public List<ciudad> findAll() {
    return (List<ciudad>) IciudadDao.findAll();
    }
}
