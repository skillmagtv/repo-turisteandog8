/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.turisteandog8.turisteandog8.services;

import com.turisteandog8.turisteandog8.modelo.experiencia;
import java.util.List;

/**
 *
 * @author nicol
 */
public interface experienciaService {
    public experiencia save(experiencia experiencia); 
    public void delete(Integer id); 
    public experiencia findById(Integer id); 
    public List<experiencia> findAll();
}
