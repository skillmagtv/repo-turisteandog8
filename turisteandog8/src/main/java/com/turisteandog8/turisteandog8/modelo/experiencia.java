/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.turisteandog8.turisteandog8.modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author nicol
 */
@Entity
@Table(name = "experiencia")
public class experiencia implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "idExperiencia")
    private Integer idExperiencia;
    @Column (name = "idTipoUsuario")
    private Integer idTipoUsuario;
    @Column (name = "fecha")
    private String fecha;
    @Column (name = "descripcion")
    private String descripcion;

    public Integer getIdExperiencia() {
        return idExperiencia;
    }

    public void setIdExperiencia(Integer idExperiencia) {
        this.idExperiencia = idExperiencia;
    }

    public Integer getTipoUsuario() {
        return idTipoUsuario;
    }

    public void setTipoUsuario(Integer tipoUsuario) {
        this.idTipoUsuario = tipoUsuario;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    
}


